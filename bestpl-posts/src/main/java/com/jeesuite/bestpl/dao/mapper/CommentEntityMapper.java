package com.jeesuite.bestpl.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.jeesuite.bestpl.dao.entity.CommentEntity;
import com.jeesuite.cache.CacheExpires;
import com.jeesuite.mybatis.plugin.cache.annotation.Cache;
import com.jeesuite.mybatis.plugin.pagination.annotation.Pageable;

import tk.mybatis.mapper.common.BaseMapper;

public interface CommentEntityMapper extends BaseMapper<CommentEntity> {
	
	@Pageable
	@Cache(expire=CacheExpires.IN_5MINS)
	@Select("SELECT * FROM sc_comment  where post_id=#{postId} order by created_at desc")
	@ResultMap("BaseResultMap")
	List<CommentEntity> findByPost(int postId);
}