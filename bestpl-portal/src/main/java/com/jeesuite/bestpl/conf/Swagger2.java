package com.jeesuite.bestpl.conf;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.async.DeferredResult;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class Swagger2 {

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("jeesuite")  
                .genericModelSubstitutes(DeferredResult.class)  
                .useDefaultResponseMessages(false)  
                .globalResponseMessage(RequestMethod.GET,customerResponseMessage())  
                .forCodeGeneration(true)  
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.jeesuite.bestpl"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("bestpl APIs")
                .description("bestpl API文档")
                .contact(new Contact("vakin", "", "vakinge@gmail.com"))
                .version("1.0")
                .build();
    }
    
    /** 
     * 自定义返回信息 
     * @param 
     * @return 
     */  
    private List<ResponseMessage> customerResponseMessage(){  
        return Arrays.asList(  
                new ResponseMessageBuilder()//500  
                        .code(500)  
                        .message("系统繁忙")  
                        .responseModel(new ModelRef("Error"))  
                        .build(),  
                new ResponseMessageBuilder()//401  
                        .code(401)  
                        .message("未授权访问")  
                        .build());  
    }  
  

}
