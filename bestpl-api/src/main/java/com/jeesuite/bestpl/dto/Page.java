package com.jeesuite.bestpl.dto;

import java.io.Serializable;
import java.util.List;

public class Page<T>  implements Serializable{

	private static final long serialVersionUID = 1L;

	//当前页
    private int pageNo = 1;
    //每页的数量
    private int pageSize = 10;
    //总记录数
    private long total;
    //总页数
    private int pages;
    //结果集
    private List<T> data;
     
	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public Page() {}
	
	public Page(int pageNo,int pageSize,long total, List<T> data) {
		this.pageNo = pageNo;
		this.pageSize = pageSize;
		this.total = total;
		this.data = data;
		this.pages = (int) ((this.total / this.getPageSize()) + (this.total % this.getPageSize() == 0 ? 0 : 1));
	}



	public long getTotal() {
		return total;
	}
	public void setTotal(long total) {
		this.total = total;
	}
	public int getPages() {
		return pages;
	}
	public void setPages(int pages) {
		this.pages = pages;
	}
	public List<T> getData() {
		return data;
	}
	public void setData(List<T> data) {
		this.data = data;
	}
    
    
}
